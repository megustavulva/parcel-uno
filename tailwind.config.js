const { colors, opacity, fontFamily } = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    extend: {
    },
    colors: {
      gray: colors.gray,
      primary: {
        100: '#ffbf9f',
        200: '#ffb48f',
        300: '#ffaa7f',
        400: '#ff9f6f',
        500: '#ff9f6f',
        600: '#ff9560',
        700: '#e58656',
        800: '#cc774c',
        900: '#b26843',
      },
      secondary: colors.indigo,
      white: '#EFEFEA'
    },
    fontSize: {
      sm: '.675rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.5rem',
      '2xl': '2rem',
      '3xl': '2.5rem',
      '4xl': '5rem'
    },
    opacity: {
      ...opacity,
      '5': '.05',
      '10': '.1'
    },
    fontFamily: {
      sans: ['Open Sans', ...fontFamily.sans],
      serif: ['Roboto Slab', ...fontFamily.serif],
      ...fontFamily
    }
  },
  container: {
    center: true,
    padding: '2rem'
  },
  variants: {
    textColor: ['hover'],
  },
  plugins: []
}
